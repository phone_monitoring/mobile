import React, {Component} from 'react'
import {
  StyleSheet,
  View,
  Text,
  Button,
  StatusBar,
  ListView,
  Alert,
  Platform,
  ToastAndroid
} from 'react-native';

import {getStudentsForClass, reportStudent} from './apiRequests'
import notify from './notify';

export default class SelectStudentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      message: ""
    };

    this.fetchStudents()
  }

  static navigationOptions = {
    title: "Schüler auswählen"
  };

  fetchStudents() {
    let {classIdentifier, token} = this.props.navigation.state.params;
    console.log("fetching students from class", classIdentifier);
    getStudentsForClass(classIdentifier, token)
      .then((response) => {
        console.log(response);
        if (response.status == 200) {
          response.json()
            .then(json => {
              console.log(json);
              const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
              return this.setState({
                dataSource: ds.cloneWithRows(json.data),
                loading: false
              })
            })
            .catch(error => {
              notify("Die Antwort des Servers war fehlerhaft.")
            })
        } else {
          notify("Der Server konnte die Anfrage nicht bearbeiten.")
        }
      })
      .catch(error => {
        notify("Es ist ein Verbindungsfehler aufgetreten.")
      })
  }

  submitSelectStudent(studentData) {
    Alert.alert(
      studentData.name + " melden ?",
      'Bitte bestätigen Sie, dass ' + studentData.name + ' gemeldet werden soll.',
      [
        {
          text: 'Abbrechen',
          onPress: () => {
            console.log('Abgebrochen')
          },
          style: 'cancel'
        },
        {
          text: 'Melden',
          onPress: () => {
            this.submitReportStudent(studentData)
          },
          style: 'destructive'
        }
      ],
      {cancelable: false}
    )
  }

  submitReportStudent(studentData) {
    let {token} = this.props.navigation.state.params;
    console.log("schüler gemeldet");
    reportStudent(studentData.id, token)
      .then(response => {
        if (response.status == 201) {
          notify(studentData.name + " erfolgreich gemeldet.")
        } else {
          notify(studentData.name + " konnte nicht gemeldet werden.")
        }
      })
      .catch(error => {
        notify("Es ist ein Verbindungsfehler aufgetreten. Bitte versuchen Sie es später erneut.")
      })
  }

  textDisplayView(text, loadingIndicator) {
    return (
      <View style={styles.displayTextView}>
        <StatusBar networkActivityIndicatorVisible={loadingIndicator}/>
        <Text style={styles.displayText}>{text}</Text>
      </View>
    )
  }

  selectionView() {
    return (
      <View style={styles.selectionView}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
                        <StudentButton
                            identifier={rowData.name}
                            onPress={() => this.submitSelectStudent(rowData)}
                        />
                    }
        />
      </View>
    );
  }

  render() {
    if (this.state.loading) {
      return this.textDisplayView("Laden...", true)
    } else if (this.state.message != "") {
      return this.textDisplayView(this.state.message, this.state.loading);
    }
    else {
      return this.selectionView()
    }
  }
}

const StudentButton = ({identifier, onPress}) => {
  return (
    <Button
      title={identifier}
      onPress={onPress}
    />
  )
};

let styles = require('./listStyle');

