import {
  Platform,
  ToastAndroid,
  Alert
} from 'react-native';

const notify = (message) => {
  if (Platform.OS == "Android") {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  } else {
    Alert.alert(
      message
    )
  }
};

export default notify;
