import {
    StackNavigator
} from 'react-navigation';

import LoginScreen from './loginScreen'
import SelectClassScreen from './selectClassScreen'
import SelectStudentScreen from './selectStudentScreen'


const App = StackNavigator({
    Login: {screen: LoginScreen},
    SelectClass: {screen: SelectClassScreen},
    SelectStudent: {screen: SelectStudentScreen}
});

export default App;
