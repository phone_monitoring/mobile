import {AsyncStorage} from 'react-native';

const apiRoot = "https://phone-monitoring-staging.herokuapp.com/api";

export function login(username, password) {
    return fetch(apiRoot + "/login", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });
}

export function getClasses(token) {
    return fetch(apiRoot + "/classes", {
        method: "GET",
        headers: {
            authorization: "Bearer " + token
        }
    })
}

export function getStudentsForClass(selectedClass, token) {
    return fetch(apiRoot + "/students?class="+selectedClass, {
        method: "GET",
        headers: {
            authorization: "Bearer " + token
        }
    })
}

export function reportStudent(id, token) {
    return fetch(apiRoot + "/infringements", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "authorization": "Bearer " + token
        },
        body: JSON.stringify({
            id: id
        })
    })
}

const STORAGE_KEY ='PhoneMonitoringAuth';

export async function saveToken(token) {
  try {

    await AsyncStorage.setItem(STORAGE_KEY, token);
  } catch (error) {
    // Error saving data
  }
}

export async function loadToken() {
  try {
    let token = await AsyncStorage.getItem(STORAGE_KEY);
    if (token !== null) {
      // We have data!!
      console.log(token);
      return {token};
    }
  } catch (error) {
  }
}