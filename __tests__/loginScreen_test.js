/**
 * Created by sebastian on 22.03.17.
 */
import 'react-native';
import React from 'react'
import LoginScreen from '../loginScreen';

import renderer from 'react-test-renderer';

test('reders correctly', () => {
  const tree = renderer.create(
    <LoginScreen/>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});