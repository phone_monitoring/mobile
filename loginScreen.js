import React, {Component} from "react";
import {Button, Platform, StatusBar, StyleSheet, Text, TextInput, View} from "react-native";

import {loadToken, login, saveToken} from "./apiRequests";

export default class LoginScreen extends Component {

  constructor(props) {
    loadToken()
      .then(promise => {
        let {navigate} = this.props.navigation;
        navigate('SelectClass', {
          token: promise.token
        });
      })
      .catch(error => {
        this.setState({checkedToken:true})
      });

    super(props);
    this.state = {
      username: "",
      password: "",
      errorMessage: "",
      loading: false,
      checkedToken: false
    };
    console.log("Try to load Token ")


  }


  static navigationOptions = {
    title: "Login"
  };

  performLogin() {
    let {navigate} = this.props.navigation;

    console.log("sending request");

    this.setState({loading: true});

    login(this.state.username, this.state.password)
      .then(response => {
        this.setState({loading: false});
        if (response.status == 200) {
          response.json()
            .then(json => {
              console.log("sucess");
              saveToken(json.token);


              this.setState({errorMessage: ""});
              navigate('SelectClass', {
                token: json.token
              });
            })
            .catch(error => this.setState({errorMessage: "Die Antwort des Servers wurde fehlerhaft übertragen."}))
        } else if (response.status == 401) {
          console.log("unauthorized");
          this.setState({errorMessage: "Falscher Nutzername oder Passwort"});
          this.password = "";
          this.focusPasswordInput()
        } else {
          this.setState({errorMessage: "Der Server konnte nicht erreicht werden."})
        }

      })
      .catch(error => {
        this.setState({loading: false});
        console.log(error);
        this.state.errorMessage = "Der Server konnte nicht erreicht werden."
      });
  };

  focusUsernameInput() {
    this.usernameInput.focus()
  }

  focusPasswordInput() {
    this.passwordInput.focus()
  }

  render() {
    if (!this.state.checkedToken){
      return <View/>
    }
    return (
      <View style={styles.view}>
        <StatusBar networkActivityIndicatorVisible={this.state.loading}/>
        <Text style={styles.list}>Username</Text>
        <TextInput
          style={styles.usernameInput}
          onChangeText={(username) => this.setState({username: username})}
          autoCapitalize="none"
          autoCorrect={false}
          autoFocus={true}
          value={this.state.username}
          returnKeyType="next"
          editable={!this.state.loading}
          ref={(input) => {
            this.usernameInput = input;
          }}
          onSubmitEditing={() => {
            this.focusPasswordInput()
          }}
        />
        <Text style={styles.list}>Password</Text>
        <TextInput
          style={styles.passwordInput}
          onChangeText={(password) => this.setState({password: password})}
          value={this.state.password}
          secureTextEntry={true}
          editable={!this.state.loading}
          returnKeyType="go"
          onSubmitEditing={() => this.performLogin()}
          ref={(input) => {
            this.passwordInput = input;
          }}
        />
        <Button
          style={styles.button}
          title="Anmelden"
          onPress={() => this.performLogin()}
          disabled={this.state.loading}
        />
        <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  usernameInput: {
    height: 40,
    ...Platform.select({
      ios: {
        borderWidth: 1,
        borderRadius: 4,
        padding: 8
      }
    })
  },
  passwordInput: {
    height: 40,
    ...Platform.select({
      ios: {
        borderWidth: 1,
        borderRadius: 4,
        padding: 8
      }
    })
  },
  view: {
    margin: 4,
  },
  button: {
    margin: 8,
  },
  errorMessage: {
    color: "red"
  }
});