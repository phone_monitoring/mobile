import { Platform, StyleSheet } from 'react-native';


module.exports = StyleSheet.create({
  ...Platform.select({
    ios: {
      displayTextView: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      displayText: {
        fontSize: 18
      },
      selectionView: {
        flex: 1
      }
    },
    android: {
      displayTextView: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      displayText: {
        fontSize: 16
      },
      selectionView: {
        flex: 1
      },
      listView: {

      },
      listViewButton:{
        backgroundColor: 'white',
        color: 'rgba(0,0,0,0.84)'


      }
    }
  })
});