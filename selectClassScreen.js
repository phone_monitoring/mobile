import React, {Component} from 'react'
import {
  StyleSheet,
  View,
  Text,
  Button,
  StatusBar,
  ListView
} from 'react-native';

import {getClasses} from './apiRequests';

export default class SelectClassScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      dataSource: null,
      message: ""
    };
    this.fetchClasses()
  }

  static navigationOptions = {
    title: "Klasse auswählen"
  };

  fetchClasses() {
    let {token} = this.props.navigation.state.params;
    console.log("fetching classes");
    getClasses(token)
      .then((response) => {
        console.log(response);
        if (response.status == 200) {
          response.json()
            .then(json => {
              console.log(json);
              const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
              return this.setState({
                dataSource: ds.cloneWithRows(json.data),
                loading: false
              });
            })
            .catch((error) => {
              console.log(error);
              this.setState({
                loading: false,
                message: "Fehler beim Holen der Daten"
              })
            })
        } else {
          this.setState({
                loading: false,
                message: "Fehler beim Holen der Daten"
              })
        }
      })
      .catch(error => {
        this.setState({
                loading: false,
                message: "Fehler beim Holen der Daten"
              })
      })
  }

  submitSelectClass(identifier) {
    let {navigate} = this.props.navigation;
    navigate('SelectStudent', {
      token: this.props.navigation.state.params.token,
      classIdentifier: identifier
    })
  }


  textDisplayView(text, loadingIndicator) {
    return (
      <View style={styles.displayTextView}>
        <StatusBar networkActivityIndicatorVisible={loadingIndicator}/>
        <Text style={styles.displayText}>{text}</Text>
      </View>
    )
  }

  selectionView() {
    return (
      <View style={styles.selectionView}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
                        <ClassButton
                            identifier={rowData}
                            onPress={() => this.submitSelectClass(rowData)}
                        />
                    }
        />
      </View>
    );
  }

  render() {
    if (this.state.loading) {
      return this.textDisplayView("Laden...", true)
    } else if (this.state.message != "") {
      return this.textDisplayView(this.state.message, this.state.loading);
    }
    else {
      return this.selectionView()
    }
  }
}

const ClassButton = ({identifier, onPress}) => {
  return (
    <Button
      title={identifier}
      onPress={onPress}
    />
  )
};
let styles = require('./listStyle');
